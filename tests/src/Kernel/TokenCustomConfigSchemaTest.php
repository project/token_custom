<?php

namespace Drupal\Tests\token_custom\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\SchemaCheckTestTrait;
use Drupal\token_custom\Entity\TokenCustomType;

/**
 * Tests the token_custom config schema.
 *
 * @group token_custom
 */
class TokenCustomConfigSchemaTest extends KernelTestBase {
  use SchemaCheckTestTrait;

  /**
   * The typed config service.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfig;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'token_custom',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->typedConfig = \Drupal::service('config.typed');
  }

  /**
   * Tests the token_custom config schema for TokenCustomType plugins.
   */
  public function testTokenCustomConfigSchema() {
    $id = 'my_token_type';
    $token_custom_type = TokenCustomType::create([
      'uuid' => '6a05119f-f3a0-43b6-bc03-070a67cb4529',
      'status' => 1,
      'dependencies' => [],
      'machineName' => $id,
      'name' => 'My Token Type',
      'description' => 'This is a custom token type for testing purposes.',
    ]);
    $token_custom_type->save();
    $config = $this->config("token_custom.type.$id");
    $this->assertEquals($config->get('machineName'), $id);
    $this->assertConfigSchema($this->typedConfig, $config->getName(), $config->get());
  }

}
