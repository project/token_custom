# Custom Tokens
----------------------

Custom Tokens gives the user the ability to create custom tokens for specific
replacements that can improve other modules relying on
<a href="http://drupal.org/project/token">Token</a>.


## Table of contents

- Requirements
- Installation
- Configuration


## Requirements

Drupal core modules

- Filter
- Text

Outside Drupal core modules

- Token


## Installation:

1. Use Composer or download Custom Tokens and Token modules.
2. Enable Custom Tokens and Token modules.
3. Assign permissions "administer custom tokens" and "administer custom token 
   types" to any role you would like to be able to manage custom tokens 
   or types.


## Configuration

- **To create tokens:**

   - Go to "Administer" -> "Structure" -> "Custom tokens".

- **To create token types:**

   - Go to "Administer" -> "Structure" -> "Custom tokens".
   - Click on Custom Token Types tab.
